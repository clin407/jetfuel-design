//@codekit-prepend './_base.js'

/* This is use to add hover function on figure/icon in Why Choose Us section */
/* Receives index of the list. 0 based index */
function whyFigureHover(index) {
	var switcher = document.querySelector(".why-us .switcher-icons");

	UIkit.switcher(switcher).show(index);
}


/* Use to add hover function on pricing blocks - Home page */
/* Receives type of pricing. standard for Standard type and premium for Premium. */
var stdPrice = document.querySelector(".pricing-container > .standard");
var prmPrice = document.querySelector(".pricing-container > .premium");
var priceBg = document.querySelector(".pricing-container > .pricing-bg");
function pricingActive(e, type) {
	if (type == "standard") {
		stdPrice.classList.add('active');
		prmPrice.classList.remove('active');
		priceBg.classList.remove('premium');
	} else {
		stdPrice.classList.remove('active');
		prmPrice.classList.add('active');
		priceBg.classList.add('premium');
	}
}


/* What We Can Do */
/* Make two slider connect */
var imageSlider = document.querySelector("#wedo-img-slider"),
    imageSliderItemsWrap = imageSlider.querySelector(".uk-slider-items");
var copyList = document.querySelector("#wedo-copy");
var squareElement = document.querySelector("#square-element"),
	squareColors = ['#dcc52f', '#cecfd9', '#f39c3c', '#fbc62e'],
	topElement = document.querySelector(".what-we-do > .top");

UIkit.util.on(imageSlider, 'itemshow', function (e) {
	setTimeout(function(){
	    for (var i = 0; i <= imageSliderItemsWrap.children.length - 1; i++) {
	        imageSliderItemsWrap.children[i].classList.contains("uk-active") ? slidersConnect(i) : "";
	    }
	}, 800);
});

function slidersConnect(index){
	for (var i = 0; i <= copyList.children.length - 1; i++) {
        copyList.children[i].classList.remove("active");
    }
    copyList.children[index].classList.add("active");

	squareElement.style.backgroundColor = squareColors[index];
	topElement.style.backgroundColor = squareColors[index];
}





var tabsContainer = document.querySelector('.tabs');
var filters = document.getElementById('filters');
var tabs = document.querySelectorAll('#filters li button');
var loadMore = document.querySelector('.load-more');
var batches = [
    {
        content: '<div class="grid-item static" style="background: url(assets/images/portfolio/pb/pb_4.png"></div><div class="grid-item web grid-item--height2" style="background: url(assets/images/portfolio/pb/pb_5.png"></div><div class="grid-item gif" style="background: url(assets/images/portfolio/pb/pb_2.png"></div><div class="grid-item videos" style="background: url(assets/images/portfolio/pb/pb_6.png"></div><div class="grid-item web" style="background: url(assets/images/portfolio/pb/pb_3.png"></div><div class="grid-item grid-item--fullwidth web" style="background: url(assets/images/portfolio/pb/pb_1.png"></div>',
        displayed: false
    },
    {
        content: '<div class="grid-item static" style="background: url(assets/images/portfolio/goop/goop_4.png"></div><div class="grid-item static grid-item--height2" style="background: url(assets/images/portfolio/goop/goop_3.png"></div><div class="grid-item gif" style="background: url(assets/images/portfolio/goop/goop_2.png"></div><div class="grid-item web" style="background: url(assets/images/portfolio/goop/goop_2.png"></div><div class="grid-item videos" style="background: url(assets/images/portfolio/goop/goop_1.png"></div><div class="grid-item grid-item--fullwidth web" style="background: url(assets/images/portfolio/goop/goop_6.png"></div>',
        displayed: false
    }
]

function isMobileDevice() {
    return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
};

function handleClick(event) {
    var active = document.querySelector('.active')

    filters.classList.toggle('open');
    if (active) active.classList.remove('active');
    event.currentTarget.classList.add('active');
}

loadMore.addEventListener( 'click', function() {
    var $items;

    for (var i = 0; i < batches.length; i++) {
        if (!batches[i].displayed) {
            // create new item elements
            $items = $(batches[i].content);
            // set displayed to true
            batches[i].displayed = true;
            break;
        }

        if (i === batches.length -2 ) {
            this.style.display = 'none';
        }
    }

    // append items to grid
    $grid.append( $items )

    // add and lay out newly appended items
    .isotope( 'appended', $items );
});

var filterFns = {
    // show if number is greater than 50
    numberGreaterThan50: function() {
        var number = this.find('.number').text();
        return parseInt( number, 10 ) > 50;
    }
};

$('#filters').on( 'click', 'button', function() {
    var filterValue = $( this ).attr('data-filter');

    // use filterFn if matches value
    filterValue = filterFns[filterValue] || filterValue;
    $grid.isotope({ filter: filterValue });
});

if (isMobileDevice()) {
    tabsContainer.classList.add('is-dropdown');
    tabs.forEach(function(tab) {
        tab.addEventListener('click', handleClick)
    });

    // listen for clicks outside the filters
    document.addEventListener('click', function(event) {
        var isOutside = !event.target.closest('#filters');
        if (isOutside) filters.classList.remove('open');
    })
} else {
    tabs.forEach(function(tab) {
        tab.addEventListener('click', handleClick)
    });
}
//@codekit-prepend './_base.js'

var priceDesktop = document.getElementsByName('pricing-desktop');
var priceMobile = document.getElementById('pricing-mobile');
var priceBlock = document.querySelectorAll('.pricing-table .pricing-block');

for (var num = priceDesktop.length - 1; num >= 0; num--) {
	priceDesktop[num].onclick = function() {
		priceSwitcher(this.value);
	}
}

priceMobile.onchange = function() {
	priceSwitcher(this.value);
}

function priceSwitcher(terms){
	for (var i = priceDesktop.length - 1; i >= 0; i--) {
		priceDesktop[i].checked = false;
	}

	document.querySelector("input[name=pricing-desktop][value="+terms+"]").checked = true;

	priceMobile.value = terms;

	for (var loop = priceBlock.length - 1; loop >= 0; loop--) {
		priceBlock[loop].classList.remove("monthly", "quarterly", "annual");
		priceBlock[loop].classList.add(terms);
	}
}
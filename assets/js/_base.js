//@codekit-prepend '../uikit/dist/js/uikit.js'
//@codekit-prepend '../uikit/dist/js/uikit-icons.js'
//@codekit-prepend './_jquery-ajax.js'

function toggleMenu() {
	var hamburger = document.querySelector(".nav-hamburger");
	var navmob = document.querySelector(".navbar-mobile");

	hamburger.classList.contains("is-active") ? hamburger.classList.remove('is-active') : hamburger.classList.add('is-active');
	navmob.classList.contains("is-active") ? navmob.classList.remove('is-active') : navmob.classList.add('is-active');
}


/* Function for input field when keyup */
function inputKeyup(e) {
	e.value != "" ? e.classList.add("active") : e.classList.remove("active");
}

/* Create an AJAX call on Submit of the Contact Form */
var form = $('#signup-form');
var submitBtn = $('#signup-form button[type=submit]');

form.submit(function(e){
	e.preventDefault();

	$.ajax({
	  xhr: function()
	  {
	    var xhr = new window.XMLHttpRequest();
	    //Upload progress
	    xhr.upload.addEventListener("progress", function(evt){
	      if (evt.lengthComputable) {
	        var percentComplete = evt.loaded / evt.total;
	        //Do something with upload progress
	        //console.log("up", percentComplete);
	        submitBtn.addClass("is-submitting");
	      }
	    }, false);
	    return xhr;
	  },
	  type: 'POST',
	  url: "/echo/json/",
	  data: {json: JSON.stringify(new Array(100000))},
	  success: function(data){
	    //console.log(arguments);
	    //Do something success-ish
	    submitBtn.removeClass("is-submitting");
	    submitBtn.addClass("is-submitted");
	  }
	});
});
